<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Factory;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\DoctrineFlusher;
use FriendsOfDdd\TransactionManager\Infrastructure\Flusher\LazyFlusherDecorator;

class LazyFlusherFactory
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function __invoke(int $maxBufferSize = 0): LazyFlusherDecorator
    {
        return new LazyFlusherDecorator(
            new DoctrineFlusher($this->entityManager),
            $maxBufferSize
        );
    }
}
