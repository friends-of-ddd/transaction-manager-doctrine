<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfDdd\TransactionManager\Application\FlusherInterface;

final class DoctrineFlusher implements FlusherInterface
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function flushOnComplete(callable $callback): void
    {
        $callback();

        $this->flush();
    }

    public function flush(): void
    {
        $this->entityManager->flush();
    }
}
