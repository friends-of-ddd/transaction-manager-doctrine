<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfDdd\TransactionManager\Domain\LogicTerminationInterface;
use FriendsOfDdd\TransactionManager\Domain\TransactionManagerInterface;
use Throwable;

final class DoctrineTransactionManager implements TransactionManagerInterface
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function wrapInTransaction(callable $callback): void
    {
        $this->entityManager->beginTransaction();

        try {
            $callback();

            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (Throwable $exception) {
            $this->entityManager->rollback();

            if (!$exception instanceof LogicTerminationInterface) {
                $this->entityManager->close();
            }

            throw $exception;
        }
    }
}
