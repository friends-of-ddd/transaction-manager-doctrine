<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Doctrine;

use Doctrine\ORM\EntityManager;
use FriendsOfDdd\TransactionManager\Application\FlusherInterface;

class DoctrineClearingFlusherDecorator implements FlusherInterface
{
    public function __construct(
        private FlusherInterface $originalFlusher,
        private EntityManager $entityManager,
    ) {
    }

    public function flushOnComplete(callable $callback): void
    {
        $this->originalFlusher->flushOnComplete($callback);
        $this->entityManager->clear();
    }

    public function flush(): void
    {
        $this->originalFlusher->flush();
        $this->entityManager->clear();
    }
}
