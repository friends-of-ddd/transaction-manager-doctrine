<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AccountBalance
{
    #[ORM\Embedded(columnPrefix: false)]
    private Amount $amount;

    public function __construct(
        #[ORM\Embedded(columnPrefix: false)]
        private AccountBalanceId $id
    ) {
        $this->amount = Amount::fromInt(0);
    }

    public function topUp(Amount $amount): self
    {
        $this->amount = $amount->add($amount);

        return $this;
    }

    public function withdraw(Amount $amount): self
    {
        $this->amount = $amount->subtract($amount);

        return $this;
    }

    public function getAmount(): Amount
    {
        return $this->amount;
    }

    public function getId(): AccountBalanceId
    {
        return $this->id;
    }
}
