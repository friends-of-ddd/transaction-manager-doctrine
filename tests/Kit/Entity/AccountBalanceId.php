<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidType;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Stringable;

#[ORM\Embeddable]
class AccountBalanceId implements Stringable
{
    private function __construct(
        #[ORM\Id]
        #[ORM\Column(type: UuidType::class)]
        private UuidInterface $id
    ) {
    }

    public static function fromString(string $id): self
    {
        return new self(Uuid::fromString($id));
    }

    public static function new(): self
    {
        return new self(Uuid::uuid4());
    }

    public function __toString(): string
    {
        return (string)$this->id;
    }
}
