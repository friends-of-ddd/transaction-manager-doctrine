<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\Entity;

use Doctrine\DBAL\Types\IntegerType;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
class Amount
{
    private function __construct(
        #[ORM\Column(type: IntegerType::class, name: 'amount')]
        private int $value
    ) {
    }

    public static function fromInt(int $amount): self
    {
        return new self($amount);
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function add(self $amount): self
    {
        return new self($this->value + $amount->getValue());
    }

    public function subtract(self $amount): self
    {
        return new self($this->value - $amount->getValue());
    }
}
