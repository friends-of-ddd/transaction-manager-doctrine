<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Functional;

use Doctrine\ORM\EntityManager;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\DoctrineClearingTransactionManagerDecorator;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Factory\ClearingTransactionManagerFactory;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\Entity\AccountBalance;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\Entity\AccountBalanceId;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\Entity\Amount;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\EntityManagerFactory;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class DoctrineClearingTransactionManagerDecoratorTest extends TestCase
{
    private EntityManager $entityManager;
    private DoctrineClearingTransactionManagerDecorator $decorator;

    protected function setUp(): void
    {
        $this->entityManager = EntityManagerFactory::create();
        $this->decorator = (new ClearingTransactionManagerFactory($this->entityManager))();
    }

    public function testEntityObjectDetachedAfterSuccessfullTransactionCommit(): void
    {
        // Arrange
        $accountBalance = new AccountBalance(AccountBalanceId::new());

        // Act
        $this->decorator->wrapInTransaction(
            fn () => $this->entityManager->persist($accountBalance)
        );

        // Assert
        $accountBalance->topUp(Amount::fromInt(100));
        $accountBalanceRepository = $this->entityManager->getRepository(AccountBalance::class);
        $savedAccountBalance = $accountBalanceRepository->find($accountBalance->getId());
        self::assertEquals($accountBalance->getId(), $savedAccountBalance->getId());
        self::assertNotSame($savedAccountBalance, $accountBalance);
        self::assertEquals(Amount::fromInt(0), $savedAccountBalance->getAmount());
    }

    public function testEntityObjectDetachedAfterTransactionRollback(): void
    {
        // Arrange
        $accountBalance = new AccountBalance(AccountBalanceId::new());
        $accountBalanceRepository = $this->entityManager->getRepository(AccountBalance::class);
        $this->entityManager->persist($accountBalance);
        $this->entityManager->flush();

        // Act
        try {
            $this->decorator->wrapInTransaction(
                fn () => throw new RuntimeException('Some failure'),
            );
        } catch (RuntimeException) {
            self::assertTrue(true);
        } catch (\Throwable $exception) {
            self::fail("Unexpected exception: $exception");
        }

        // Assert
        $accountBalance->topUp(Amount::fromInt(100));
        $savedAccountBalance = $accountBalanceRepository->find($accountBalance->getId());
        self::assertEquals($accountBalance->getId(), $savedAccountBalance->getId());
        self::assertNotSame($savedAccountBalance, $accountBalance);
        self::assertEquals(Amount::fromInt(0), $savedAccountBalance->getAmount());
    }
}
