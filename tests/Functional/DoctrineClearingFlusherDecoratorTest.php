<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Functional;

use Doctrine\ORM\EntityManager;
use FriendsOfDdd\TransactionManager\Application\FlusherInterface;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Factory\LazyFlusherWithClearingFactory;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\Entity\AccountBalance;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\Entity\AccountBalanceId;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\Entity\Amount;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\EntityManagerFactory;
use PHPUnit\Framework\TestCase;

class DoctrineClearingFlusherDecoratorTest extends TestCase
{
    private EntityManager $entityManager;
    private FlusherInterface $flusher;

    protected function setUp(): void
    {
        $this->entityManager = EntityManagerFactory::create();
        $this->flusher = (new LazyFlusherWithClearingFactory($this->entityManager))();
    }

    public function testEntityBecomesDetachedAfterForceFlush(): void
    {
        // Arrange
        $accountBalance = new AccountBalance(AccountBalanceId::new());
        $this->entityManager->persist($accountBalance);

        // Act
        $this->flusher->flush();

        // Assert
        $accountBalance->topUp(Amount::fromInt(100));
        $accountBalanceRepository = $this->entityManager->getRepository(AccountBalance::class);
        $savedAccountBalance = $accountBalanceRepository->find($accountBalance->getId());
        self::assertEquals($accountBalance->getId(), $savedAccountBalance->getId());
        self::assertNotSame($savedAccountBalance, $accountBalance);
        self::assertEquals(Amount::fromInt(0), $savedAccountBalance->getAmount());
    }

    public function testEntityBecomesDetachedAfterFlushOnComplete(): void
    {
        // Arrange
        $accountBalance = new AccountBalance(AccountBalanceId::new());

        // Act
        $this->flusher->flushOnComplete(
            fn () => $this->entityManager->persist($accountBalance)
        );

        // Assert
        $accountBalance->topUp(Amount::fromInt(100));
        $accountBalanceRepository = $this->entityManager->getRepository(AccountBalance::class);
        $savedAccountBalance = $accountBalanceRepository->find($accountBalance->getId());
        self::assertEquals($accountBalance->getId(), $savedAccountBalance->getId());
        self::assertNotSame($savedAccountBalance, $accountBalance);
        self::assertEquals(Amount::fromInt(0), $savedAccountBalance->getAmount());
    }
}
