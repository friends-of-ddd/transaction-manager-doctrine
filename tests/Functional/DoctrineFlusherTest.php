<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Functional;

use Doctrine\ORM\EntityManager;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Factory\LazyFlusherFactory;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\Entity\AccountBalance;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\Entity\AccountBalanceId;
use FriendsOfDdd\TransactionManager\Infrastructure\Doctrine\Tests\Kit\EntityManagerFactory;
use PHPUnit\Framework\TestCase;

class DoctrineFlusherTest extends TestCase
{
    private EntityManager $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = EntityManagerFactory::create();
    }

    public function testLazyDecoratorPersistInCallbackAtThreeLevelsGetsFlushedAutomatically(): void
    {
        // Arrange
        $accountBalances = [
            new AccountBalance(AccountBalanceId::new()),
            new AccountBalance(AccountBalanceId::new()),
            new AccountBalance(AccountBalanceId::new()),
        ];
        $accountBalanceRepository = $this->entityManager->getRepository(AccountBalance::class);

        $lazyFlusher = (new LazyFlusherFactory($this->entityManager))();

        // Act
        $lazyFlusher->flushOnComplete(function () use ($accountBalances, $lazyFlusher) {
            $this->entityManager->persist($accountBalances[0]);

            $lazyFlusher->flushOnComplete(function () use ($accountBalances, $lazyFlusher) {
                $this->entityManager->persist($accountBalances[1]);

                $lazyFlusher->flushOnComplete(function () use ($accountBalances) {
                    $this->entityManager->persist($accountBalances[2]);
                });
            });
        });

        // Assert
        $this->entityManager->clear();
        foreach ($accountBalances as $accountBalance) {
            $savedAccountBalance = $accountBalanceRepository->find($accountBalance->getId());

            self::assertEquals($accountBalance->getAmount(), $savedAccountBalance->getAmount());
        }
    }
}
